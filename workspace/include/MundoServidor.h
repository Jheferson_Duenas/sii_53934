// MundoServidor.h: interface for the CMundo class.
//Código modificado por Jheferson Dueñas. 53934

//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
//#include <DatosMemCompartida.h>
#include "pthread.h"
#include "Socket.h"

class CMundo_s 
{
public:
	void Init();
	CMundo_s();
	virtual ~CMundo_s();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int fd;//descriptor de fichero de la FIFO
	//DatosMemCompartida datosCompartidos; // atributo de datos en memoria compartidos
	//DatosMemCompartida *pdatosCompartidos; // puntero a datos en memoria compartidos

	int puntos1;
	int puntos2;
	
	//int fd_sc;
	//int fd_cs;
	
	pthread_t thid1;
	//pthread_attr_t atrib;
	Socket s_conexion;
	Socket s_comunicacion;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
