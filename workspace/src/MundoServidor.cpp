// MundoServidor.cpp: implementation of the CMundo class.
//Código modificado por Jheferson Dueñas. 53934

//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <string>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <Puntuaciones.h>
#include "signal.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo_s::CMundo_s()
{
	Init();
}

CMundo_s::~CMundo_s()
{
	close(fd);
	//unlink("/tmp/MI_FIFO");
	//munmap(pdatosCompartidos,sizeof(datosCompartidos));
	//close(fd_sc);
}

void* hilo_comandos(void* d)
{
	CMundo_s* p=(CMundo_s*) d;
	p->RecibeComandosJugador();
}

void CMundo_s::RecibeComandosJugador()
{
//Inicialización de la FIFO.Comunicación cliente servidor
	/*fd_cs=open("/tmp/FIFO_cliente_servidor",O_RDONLY);
	if(fd_cs<0){
		perror("MundoServidor: No se puede abrir FIFO_CS");
    		exit (1);
	}*/
	while(1){
		usleep(10);
		char cad[100];
		//read(fd_cs,cad,sizeof(cad));
		int recept;
		recept = s_comunicacion.Receive(cad,sizeof(cad));
		if(recept < 0){
                perror("MundoServidor: Error Receive en servidor");
                exit (1);
	        }

		unsigned char key;
		sscanf(cad,"%c",&key);
		if (key=='s') jugador1.velocidad.y=-4;
		if (key=='w') jugador1.velocidad.y=4;
		if (key=='l') jugador2.velocidad.y=-4;
		if (key=='o') jugador2.velocidad.y=4;
	}
}

void CMundo_s::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_s::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_s::OnTimer(int value)
{	
	Puntuaciones puntos;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Mueve();
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWin=2;
		write(fd,&puntos,sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWin=1;
		write(fd,&puntos,sizeof(puntos));
	}
//Actualización de datos en memoria compartidos
	//pdatosCompartidos->esfera=esfera;
	//pdatosCompartidos->raqueta1=jugador1;
	
	
	/*if(pdatosCompartidos->accion==-1){OnKeyboardDown('s',0,0);}
	else if(pdatosCompartidos->accion==0){}
	else if(pdatosCompartidos->accion==1){OnKeyboardDown('w',0,0);}
	if(puntos1==3 || puntos2==3){
		if(puntos1==3)
		{
		printf("Ha ganado el jugador 1. Ha sido el primero en marcar 3 puntos.\n");
		}
		else if(puntos2==3)
		{
		printf("Ha ganado el jugador 2. Ha sido el primero en marcar 3 puntos.\n");
		}
		pdatosCompartidos->bit_fin=1;
		exit (0);

	}*/
//Envío de coordenadas del servidor al cliente	
	char cad[200];	
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);	
	//write(fd_sc,cad,sizeof(cad));
	int envio;
	envio =	s_comunicacion.Send(cad,sizeof(cad));
	if(envio < 0){
                perror("MundoServidor: Error envío en servidor");
                exit (1);
        }

}

void CMundo_s::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void TerminarProceso(int sign)
{
	printf("\nSeñal capturada %s (%d)\n",strsignal(sign),sign);
	if (sign==SIGUSR2)	
		exit(0);
	else 
		exit(sign);
	
}
void CMundo_s::Init()
{
//Inicialización de la FIFO.Comunicación con logger
	fd=open("/tmp/MI_FIFO",O_WRONLY);
	if(fd<0){
		perror("MundoServidor: No se puede abrir MI_FIFO");
		exit (1);
	}
//Conexión del SOCKET
	char ip[]="127.0.0.1";
	int conect;
	int recept;
	conect = s_conexion.InitServer(ip,8000);
	if(conect < 0){
		perror("MundoServidor: Error abriendo servidor");
		exit (1);
	}
	s_comunicacion = s_conexion.Accept();
	//Recibir nombre del cliente
	char nombre[50];
	recept = s_comunicacion.Receive(nombre,sizeof(nombre));
	if(recept < 0){
                perror("MundoServidor: Error Receive en servidor");
                exit (1);
	}
	printf("%s se ha conectado.\n",nombre);
//Inicialización de la FIFO.Comuniccación servidor-cliente
	/*fd_sc=open("/tmp/FIFO_servidor_cliente",O_WRONLY);
	if(fd_sc<0){
		perror("MundoServidor: No se puede abrir FIFO_servidor_cliente");
		exit (1);
	}*/
	
//Threads//////////////////////////
	/*Pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib,PTHREAD_CREATE_JOINABLE);*/
	pthread_create(&thid1, NULL, hilo_comandos, this);

///Armado de señales/////////////////////////////////////
	struct sigaction accion;
	accion.sa_handler=TerminarProceso;
	accion.sa_flags= SA_RESTART;
	int act;
	//sigemptyset(&accion.sa_mask);
	act=sigaction(SIGINT,&accion,NULL);
	if(act<0){
		perror("SIGINT");
		exit (1);
	}
	act=sigaction(SIGTERM,&accion,NULL);
	if(act<0){
		perror("SIGTERM");
		exit (1);
	}
	act=sigaction(SIGPIPE,&accion,NULL);
	if(act<0){
		perror("SIGPIPE");
		exit (1);
	}
	act=sigaction(SIGUSR2,&accion,NULL);
	if(act<0){
		perror("SIGUSR2");
		exit (1);
	}
//////////////////////////////////////
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}


