#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include <Puntuaciones.h>

int main(){
int fd,make,undo,n_bytes=0;
Puntuaciones puntos;
	unlink("/tmp/MI_FIFO");
    /*Crear el FIFO*/
    make=mkfifo("/tmp/MI_FIFO",0600);
    if (make<0){
     	perror("logger: No se puede crear FIFO");
     	return 1;    
    }
    /*Abrir el FIFO*/
    fd=open("/tmp/MI_FIFO", O_RDWR);
    if (fd<0){
    	perror("logger: No se puede abrir FIFO");
    	return 1;
    }
    
    while((puntos.jugador1 != 3)|(puntos.jugador2 != 3)){
    	n_bytes=read(fd,&puntos,sizeof(puntos));
	if(n_bytes != sizeof(puntos)){
          perror("logger: Error de lectura de la FIFO");
	  return 1;
	  }    
    	if (puntos.lastWin==1){
    		printf("Jugador 1 marca 1 punto. lleva un total de %d puntos.\n",puntos.jugador1);
    	}
    	else if (puntos.lastWin==2){
    		printf("Jugador 2 marca 1 punto. lleva un total de %d puntos.\n",puntos.jugador2);
    	}
    }
close (fd);
undo=unlink("/tmp/MI_FIFO");
if(undo<0){
	perror("logger: No se puede eliminar FIFO");
	return 1;
}
return 0;
        
}

