#include <DatosMemCompartida.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(){
	//unlink("/tmp/datos_Compartidos");
	int fd;
	DatosMemCompartida *pdatosCompartidos;
	fd=open("/tmp/datos_Compartidos",O_RDWR);
	if(fd<0){
		perror("bot: No se puede abrir el fichero");
		return 1;
	}
	pdatosCompartidos=static_cast<DatosMemCompartida*> (mmap(0,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0));
	if(pdatosCompartidos==MAP_FAILED){
		perror("bot: Error al proyectar en memoria");
		close(fd);
		return 1;
	}
	close(fd);
	while((pdatosCompartidos->bit_fin) == 0){ 
		if(pdatosCompartidos->esfera.centro.y < pdatosCompartidos->raqueta1.getCentro().y){
		pdatosCompartidos->accion=-1; //raqueta 1 hacia abajo
		}
		else if(pdatosCompartidos->esfera.centro.y > pdatosCompartidos->raqueta1.getCentro().y){
		pdatosCompartidos->accion=1; //raqueta 1 hacia arriba
		}
		else 
		pdatosCompartidos->accion=0; //raqueta 1 no se mueve
		
		usleep(25000); //en suspensión 25 ms
	}
munmap(pdatosCompartidos,sizeof(DatosMemCompartida));
unlink("/tmp/datos_Compartidos");
return 0;
}
