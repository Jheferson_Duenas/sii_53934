// Mundo.cpp: implementation of the CMundo class.
//Código modificado por Jheferson Dueñas. 53934

//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <string>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <Puntuaciones.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo_c::CMundo_c()
{
	Init();
}

CMundo_c::~CMundo_c()
{
	//close(fd_sc);
	//unlink("/tmp/FIFO_servidor_cliente");
	//close(fd_cs);
	//unlink("/tmp/FIFO_cliente_servidor");
	munmap(pdatosCompartidos,sizeof(datosCompartidos));
}

void CMundo_c::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo_c::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo_c::OnTimer(int value)
{	
	Puntuaciones puntos;
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Mueve();
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWin=2;
		//write(fd,&puntos,sizeof(puntos));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1=puntos1;
		puntos.jugador2=puntos2;
		puntos.lastWin=1;
		//write(fd,&puntos,sizeof(puntos));
	}
//Actualización de datos en memoria compartidos
	pdatosCompartidos->esfera=esfera;
	pdatosCompartidos->raqueta1=jugador1;
	
	
	if(pdatosCompartidos->accion==-1){OnKeyboardDown('s',0,0);}
	else if(pdatosCompartidos->accion==0){}
	else if(pdatosCompartidos->accion==1){OnKeyboardDown('w',0,0);}
	if(puntos1==3 || puntos2==3){
		if(puntos1==3)
		{
		printf("Ha ganado el jugador 1. Ha sido el primero en marcar 3 puntos.\n");
		}
		else if(puntos2==3)
		{
		printf("Ha ganado el jugador 2. Ha sido el primero en marcar 3 puntos.\n");
		}
		pdatosCompartidos->bit_fin=1;
		exit (0);
	}
//Envío de coordenadas del servidor al cliente	
	char cad[200];
	int recept;	
	//read(fd_sc,cad,sizeof(cad));
	recept = s_comunicacion.Receive(cad,sizeof(cad));
	if(recept < 0){
                perror("MundoCliente: Error Receive en cliente");
                exit (1);
        }

	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);	
	
}

void CMundo_c::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[100];
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':sprintf(cad,"s");break;
	case 'w':sprintf(cad,"w");break;
	case 'l':sprintf(cad,"l");break;
	case 'o':sprintf(cad,"o");break;

	}
	
	//Envío de teclas cliente-servidor
	//char* cad;	
	//sprintf(cad,"%s",(char)key);	
	//write(fd_cs,&key,sizeof(key));
	int envio;
	envio =	s_comunicacion.Send(cad,sizeof(cad));
	if(envio < 0){
                perror("MundoCliente: Error envío en cliente");
                exit (1);
        }

}

void CMundo_c::Init()
{
	//unlink("/tmp/FIFO_servidor_cliente");
	//unlink("/tmp/FIFO_cliente_servidor");

//BOT
//Creación del fichero de datos de memoria compartida
	int fd_mmap,n_bytes,make1,make2;
	//int id_stat;	
	//struct stat st;
	fd_mmap=open("/tmp/datos_Compartidos",O_CREAT|O_TRUNC|O_RDWR,0666);
	if(fd_mmap<0){
		perror("MundoCliente: No se pudo crear un fichero de datos compartidos");
		exit (1);
	}	
	n_bytes=write(fd_mmap,&datosCompartidos,sizeof(datosCompartidos));
	
	/*id_stat=fstat(fd_mmap,&st);
	if(id_stat<0){
		perror("MundoCliente: No se pudo obtener la información del fichero de datos compartidos");
		exit (1);
	}*/
	
	pdatosCompartidos=static_cast <DatosMemCompartida*> (mmap(0,sizeof(datosCompartidos),PROT_READ|PROT_WRITE,MAP_SHARED,fd_mmap,0));
	
	if(pdatosCompartidos==MAP_FAILED){
		close(fd_mmap);
		perror("MundoCliente: Error al proyectar el fichero de datos compartidos");	
		exit (1);
	}

	close(fd_mmap);
	pdatosCompartidos->bit_fin=0;
	
//Conexión con servidor
	char ip[]="127.0.0.1";
	char nombre[50];
	printf("Introduzca su nombre: \n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);
	//Envío del nombre al servidor
	int envio;
	envio =	s_comunicacion.Send(nombre,sizeof(nombre));
	if(envio < 0){
                perror("MundoCliente: Error envío en cliente");
                exit (1);
        }

	
//Comunicación servidor-cliente
	/*make1=mkfifo("/tmp/FIFO_servidor_cliente",0600);
	if(make1<0){
	perror("MundoCliente: No se puede crear FIFO_SC");
     	exit (1); 
	}
	
	fd_sc=open("/tmp/FIFO_servidor_cliente",O_RDONLY);
	 if (fd_sc<0){
    	perror("MundoCliente: No se puede abrir FIFO_SC");
    	exit (1);
    }
//Comunicación cliente-servidor
	make2=mkfifo("/tmp/FIFO_cliente_servidor",0600);
	if(make2<0){
	perror("MundoCliente: No se puede crear FIFO_CS");
     	exit (1); 
	}
	
	fd_cs=open("/tmp/FIFO_cliente_servidor",O_WRONLY);
	 if (fd_cs<0){
    	perror("MundoCliente: No se puede abrir FIFO_CS");
    	exit (1);
    }*/
/////////////////////////////////////////
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	

}
